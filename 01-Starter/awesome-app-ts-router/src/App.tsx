import React, { lazy, Suspense } from 'react';
import { Link, Router } from '@reach/router';
import './App.scss';

// Note: code-splitting with dynamic import() is optional. The componets could also be imported statically.
const About = lazy(() => import('./about/About'));
const Home = lazy(() => import('./home/Home'));

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
        </ul>
      </header>
      <div className="App-body">
        <Suspense fallback={<h3>Loading ...</h3>}>
          <Router>
            <Home path="/" />
            <About path="about" />
          </Router>
        </Suspense>
      </div>
    </div>
  );
};

export default App;
