import React from 'react';
import { RouteComponentProps } from "@reach/router";

type HomeProps = RouteComponentProps;

const Home: React.FC<HomeProps> = () => {
  return (
    <div >
      <h1>Home</h1>
      <p>This is the Home component.</p>
    </div>
  );
}

export default Home;
