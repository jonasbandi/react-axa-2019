import React from 'react';
import { RouteComponentProps } from "@reach/router";
import styles from './About.module.scss';

type AboutProps = RouteComponentProps;

const About: React.FC<AboutProps> = () => {
  return (
    <div className={styles.comic}>
      <h1>About</h1>
      <p>This is the About component.</p>
    </div>
  );
};

export default About;
