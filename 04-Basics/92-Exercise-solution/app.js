const {useState, useEffect} = React;

function App() {
  // The App component is a container component
  // It's primary focus is to manage the data and delegate the rendering to presentation components

  const [champions, setChampions] = useState([]);

  useEffect(() => {
    const initialChampions = loadChampions();
    setChampions(initialChampions);
  }, []);


  function incrementLikeOnChampion(champion) {
    const championIndex = champions.indexOf(champion);
    if (championIndex >= 0) {
      // state should not be modified
      const changedChampions = champions.map(c => c !== champion ? c : {
        ...champion,
        likes: champion.likes + 1
      });
      setChampions(changedChampions);
    }
  }

  return (
    <ChampionList
      champions={champions}
      onLike={incrementLikeOnChampion}
    />
  );
}

/****** Presentation Components ******/

function ChampionList({champions, onLike}) {
  return (
    <div className="pa3">
      <ListTitle title="Rate the Champions!"/>
      <ul className="list pl0 ml0 center mw6 ba b--light-silver br2">
        {champions.map((champion, index) => (
          <Champion
            champion={champion}
            onLike={() => onLike(champion)}
            key={index}
          />
        ))}
      </ul>
    </div>
  );
}

function ListTitle({title}) {
  return <h1 className="f4 bold center mw6">{title}</h1>;
}

function Champion({champion, onLike}) {
  return (
    <li className="dt w-100 ph3 pv3 bb b--light-silver">
      <div className="dtc v-mid">
        <div>{champion.name}</div>
        <div className="pv2">👍 {champion.likes}</div>
      </div>
      <div className="dtc v-mid">
        <div className="tr">
          <button
            className="f6 button-reset bg-white ba b--black-10 dim pointer pv1 black-60"
            onClick={onLike}
          >
            Like!
          </button>
        </div>
      </div>
    </li>
  );
}

ReactDOM.render(<App/>, document.getElementById("root"));

function loadChampions() {
  return [
    {name: "Katniss", likes: 0},
    {name: "Peeta", likes: 0},
    {name: "Johanna", likes: 0},
    {name: "Haymich", likes: 0}
  ];
}
