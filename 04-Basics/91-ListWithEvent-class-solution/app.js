const Title = props => <h1>{props.title}</h1>;

class Clock extends React.Component {
  state = {
    time: new Date()
  };

  refreshTime = () => {
    this.setState({ time: new Date() });
  };

  render() {
    const displayTime = moment(this.state.time)
      .add(this.props.hourOffset, 'h')
      .toString();
    return (
      <div>
        <span>{displayTime}</span>
        <button onClick={this.refreshTime}>Refresh!</button>
        <br />
        <br />
      </div>
    );
  }
}

function AppComponent({ title, countries }) {
  return (
    <div>
      <Title title={title} />
      <br />
      {countries.map(country => (
        <div key={country.id}>
          Time in {country.name}: <Clock hourOffset={country.offset} />
        </div>
      ))}
    </div>
  );
}

const app = <AppComponent title="Greetings" countries={getCountries()} />;

ReactDOM.render(app, document.getElementById('root'));

function getCountries() {
  return [
    { id: 'fr', name: 'France', offset: 0 },
    { id: 'de', name: 'Germany', offset: 0 },
    { id: 'it', name: 'Italy', offset: 0 },
    { id: 'id', name: 'India', offset: -5 },
    { id: 'in', name: 'Indonesia', offset: -7 },
    { id: 'ch', name: 'Switzerland', offset: 0 },
    { id: 'us', name: 'USA', offset: 8 }
  ];
}
