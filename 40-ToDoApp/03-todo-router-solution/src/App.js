import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, NavLink, Route, Switch } from 'react-router-dom';
// import PendingToDos from './components/PendingToDos';
// import DoneToDos from './components/DoneToDos';
// import ToDoDetail from './components/ToDoDetail';

const PendingToDos = lazy(() => import('./components/PendingToDos'));
const DoneToDos = lazy(() => import('./components/DoneToDos'));
const ToDoDetail = lazy(() => import('./components/ToDoDetail'));

function App() {
  return (
    <div className="App">
      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <section className="todoapp">
        <Router>
          <div>
            <div className="nav">
              <NavLink exact to="/" activeClassName="selected">
                Pending
              </NavLink>
              <NavLink exact to="/done" activeClassName="selected">
                Done
              </NavLink>
            </div>
            <Suspense fallback={<div>Loading ...</div>}>
              <Switch>

                {/*react-router 4.3 logs a warning when '<Route component={}' is used with React.lazy ...*/}
                {/*this should be fixed in react-router 4.4: https://github.com/ReactTraining/react-router/issues/6420*/}
                {/*Until then we use the render prop of Route*/}

                <Route path="/detail/:id" render={(params) => <ToDoDetail {...params}/>} />
                <Route path="/done" render={(params) => <DoneToDos {...params}/>} />
                <Route path="/" render={(params) => <PendingToDos {...params}/>} />

              </Switch>
            </Suspense>
          </div>
        </Router>
      </section>
      <footer className="info">
        <p>
          JavaScript Example / Initial template from{' '}
          <a href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
