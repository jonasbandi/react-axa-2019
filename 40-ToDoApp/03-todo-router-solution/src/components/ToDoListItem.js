import React from 'react';
import {PropTypes} from 'prop-types';
import {NavLink} from 'react-router-dom';

function ToDoListItem ({todo, onRemoveToDo}){
  return (
    <li key={todo.id}>
      {todo.title}
      <NavLink to={`/detail/${todo.id}`} style={{fontSize: 'small'}}>✏️</NavLink>
      <button onClick={() => onRemoveToDo(todo)}>X</button>
    </li>
  );
}

ToDoListItem.propTypes = {
    todo: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired
    }).isRequired,
    onRemoveToDo: PropTypes.func.isRequired
};

export default ToDoListItem;
