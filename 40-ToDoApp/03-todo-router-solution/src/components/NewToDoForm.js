import React from 'react';
import {PropTypes} from 'prop-types'

NewToDoForm.propTypes = {
  onAddToDo: PropTypes.func.isRequired
};

export default function NewToDoForm({onAddToDo}) {

  const [toDoTitle, setToDoTitle] = useState('');

  function formChange(e) {
    setToDoTitle(e.currentTarget.value);
  }

  function addToDo(e) {
    e.preventDefault();
    onAddToDo(toDoTitle);
    setToDoTitle('');
  }

  return (
    <form className="new-todo" onSubmit={addToDo}>
      <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
             autoFocus
             autoComplete="off"
             value={toDoTitle}
             onChange={formChange}
      />

      <button id="add-button" className="add-button">+</button>
    </form>
  );
}
