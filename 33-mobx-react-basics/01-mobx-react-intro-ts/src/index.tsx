import React from 'react';
import ReactDOM from 'react-dom';
import { autorun } from 'mobx';
import { TodoStore } from './store';
import { App } from './App';

const store = new TodoStore();
window['store'] = store;


autorun(() => render());

function render() {
    ReactDOM.render(
        <App store={store}/>,
        document.getElementById('root')
    );
}


store.addTodo('Learn JavaScript!');
store.addTodo('Learn React');
store.addTodo('Learn MobX');

// DEMO: run on the console:
// store.addTodo('Learn TypeScript');
// store.removeTo(0);
// store.removeTo(0);
//
// Add @observer to the App component and remove autorun


