function Advertisement() {
  return <div style={{height: 100, backgroundColor: 'yellow'}}>Advertisement</div>
}
function MainContent() {
  return <div style={{height: 100, backgroundColor: 'cyan'}}>Main Content</div>
}
function Navigation() {
  return <div style={{height: 100, backgroundColor: 'pink'}}>Navigation</div>
}

function Layout({leftComponent, middleComponent, rightComponent}) {
  return (
    <div style={{display: 'flex', justifyContent: 'space-around'}}>
      <div>{leftComponent}</div>
      <div>{middleComponent}</div>
      <div>{rightComponent}</div>
    </div>
  );
}


function App() {
  return (
    <Layout
      leftComponent={<Navigation/>}
      middleComponent={<MainContent/>}
      rightComponent={<Advertisement/>}/>
  );
}

ReactDOM.render(<App/>, document.getElementById("root"));

