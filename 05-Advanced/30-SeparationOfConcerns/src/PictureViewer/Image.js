import React from 'react';

function Image({imageUrl}) {
  return <img src={imageUrl} style={{width: 400}}/>;
}

export default Image;
