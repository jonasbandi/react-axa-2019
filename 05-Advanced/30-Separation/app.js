const {useReducer, useRef} = React;


function View({loading, onLoad, imageUrl}) {

  let content;
  if (loading) {
    content = <div>loading ...</div>;
  }
  else if (imageUrl){
    content = <img src={imageUrl}/>
  }

  return (
    <>
      <button onClick={onLoad}>Next</button>
      {content}
    </>
  )
}


function App() {

  const [state, dispatch] = useReducer(reducer, {imageUrl: undefined, loading: false});

  async function loadImage() {
    console.log('Searching');
    dispatch({type: 'LOADING_START'});
    const response = await fetch(`https://api.thecatapi.com/v1/images/search`);
    const data = await response.json();
    const url = data[0].url;
    console.log('URL', url);
    dispatch({type: 'LOADING_SUCCESS', payload: url})
  }

  return <View loading={state.loading} imageUrl={state.imageUrl} onLoad={loadImage}/>

}

function reducer(state, action) {
  switch (action.type) {
    case 'LOADING_START':
      return {...state, loading: true};
    case 'LOADING_SUCCESS':
      return {...state, imageUrl: action.payload, loading: false};
    default:
      throw new Error();
  }
}

ReactDOM.render(<App/>, document.getElementById("root"));

